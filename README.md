# Tamagopg

## Comment jouer

### Choix du personnage

Lors de votre arrivé sur la page principale, vous devrez remplir un petit formulaire 
pour déterminer votre personnage et ainsi vous aventurez dans le donjon.

#### Les personnages jouable

- **Guerrier** : Téméraire, Fort et Respectable, il sera subir les dégâts.
- **Magicien** : Mystérieux, Calme et Spéctaculaire, faites tourner en bourrique vos ennemis.
- **Assassin** : Silencieux, Vif et Rapide, attaquer la ou ça fait mal.


### Avancer dans le jeu

Une fois votre personnage définit, des monstres essaieront de vous bloquer le chemin,
gérer vos HP, votre équipe et faites-leur face !

**Jusqu'ou pourrez vous aller ?**

### Gagner ou perdre

- Vous perdez si vos HP de votre héro tombe à 0. 
- Vous gagnez quand vous avez fini tous les donjons.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
