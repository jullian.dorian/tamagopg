import {Entity} from "./entity";
import {clamp} from "../tmath";

import iwarrior from "../../assets/img/warrior.png"
import image from "../../assets/img/mage.png"
import islayer from "../../assets/img/slayer.png"

export class Characters {
    static TYPES = {
        WARRIOR: 0,
        MAGE: 1,
        SLAYER: 2
    }

    static calculate(responses) {
        let warrior= 0, mage= 0, slayer= 0;

        responses.forEach(r => {
            if(r === 0)
                warrior += 1;
            else if(r === 1)
                mage += 1;
            else
                slayer += 1;
        });

        if(warrior > mage && warrior > slayer) return Warrior;
        else if(mage > warrior && mage > slayer) return Mage;
        else return Slayer;
    }
}

export class Character extends Entity {

    energy = 100;
    maxEnergy = 100;
    hunger = 100;
    maxHunger = 100;

    factorEnergy = 0.25;
    factorHunger = 0.50;

    imgUrl = "";

    constructor(name, health, damage, attack_speed = 3) {
        super(name, health, damage, attack_speed);

        this.attributes();
    }

    updateGeneric(ms) {
        if(this.alive) {
            this.energy -= ms / 1000 * this.factorEnergy;
            this.hunger -= ms / 1000 * this.factorHunger;

            this.energy = clamp(this.energy, 0, this.maxEnergy);
            this.hunger = clamp(this.hunger, 0, this.maxHunger);

            if (this.energy <= 0.00 || this.health <= 0.00) {
                this.alive = false;
            }

            if (this.hunger <= 0.00) {
                this.health -= clamp(ms / 10, 0.10, 0.20);
                this.health = clamp(this.health, 0, this.maxHealth);
            }
        }
    }

    eat() {
        if(this.alive) {
            this.energy = clamp(this.energy + 10, 0, this.maxEnergy);
            this.hunger = clamp(this.hunger + 75, 0, this.maxHunger);
        }
    }

    rest() {
        if(this.alive) {
            this.energy = clamp(this.energy + 75, 0, this.maxEnergy);
            this.hunger = clamp(this.hunger - 15, 0, this.maxHunger);
        }
    }

    heal() {
        if(this.alive) {
            this.energy = clamp(this.energy - 15, 0, this.maxEnergy)
            this.health = clamp(this.health + 5, 0, this.maxHealth);
        }
    }

    upgrade () {
        this.health += 2;
        this.maxHealth += 2;
    }
}

export class Warrior extends Character {

    constructor(name, health, damage) {
        super(name, health, damage);
        this.imgUrl = iwarrior;

    }

    upgrade() {
        super.upgrade();
        this.damage += 2;
    }
}

export class Mage extends Character {

    mana = 0;
    maxMana = 0;

    constructor(name, health, damage, mana) {
        super(name, health, damage);
        this.maxMana = mana;
        this.mana = mana;
        this.imgUrl = image;

        this.factorEnergy = 0.20;
    }

    attributes() {
        let d = super.attributes();
        d.push({
            name: "Mana",
            value: this.mana
        });
        return d;
    }

    attackdamage(entity, damage) {
        if(this.mana >= 2) {
            this.mana -= 2;

            super.attackdamage(entity, damage);
        }
    }

    canAttack() {
        return super.canAttack() && this.mana > 0;
    }

    reset() {
        super.reset();
        this.mana = this.maxMana;
    }

    upgrade() {
        super.upgrade();

        this.maxMana += 5;
        this.mana += 5;

        let randDmg = Math.random();
        if(randDmg <= 0.15) {
            this.damage += 2;
        } else if(randDmg <= 0.40) {
            this.damage += 1;
        }
    }

}

export class Slayer extends Character {

    constructor(name, health, damage, attack_speed = 1) {
        super(name, health, damage, attack_speed);

        this.factorExhaust = 0.40;

        this.imgUrl = islayer;
    }

    upgrade() {
        super.upgrade();

        this.speed_attack -= 0.05;

        let randDmg = Math.random();
        if(randDmg <= 0.15) {
            this.damage += 2;
        } else if(randDmg <= 0.40) {
            this.damage += 1;
        }
    }
}