import {affine} from '../tmath'

export class Entity {

    health;
    maxHealth;
    name;
    alive = true;
    damage;
    speed_attack;

    next_timing_attack = 0;

    ambiant;

    constructor(name, health, damage, speed_attack = 3) {
        this.maxHealth = this.health = health;
        this.name = name;
        this.damage = damage;
        this.speed_attack = speed_attack;
        this.next_timing_attack = speed_attack * 1000;
    }

    attributes() {
        let d = [
            {
                name: "Dégâts",
                value: affine(this.damage)
            },
            {
                name: "Vitesse d'attaque",
                value: affine(this.speed_attack) + "s"
            }
        ]

        return d;
    }

    update(ms) {
        if(this.alive) {
            if (this.next_timing_attack <= 0.00) {
                this.next_timing_attack = this.speed_attack * 1000;
            } else {
                this.next_timing_attack -= ms;
            }
        }
    }

    /**
     * @param entity {Entity|Entity[]}
     * @param damage {Number}
     */
    attack(entity) {

        if(Array.isArray(entity)) {

            let alives = [];
            entity.forEach(e => {
                if(e.alive) {
                    alives.push(e);
                }
            });

            let index = Math.floor(Math.random() * (alives.length));
            let ent = alives[index];

            if(ent) {
                this.attack(ent);
            }
            return;
        }

        this.attackdamage(entity, this.damage);

        this.attributes();
    }

    attackdamage(entity, damage) {
        let old = entity.health;
        if(old - damage <= 0.00) {
            entity.health = 0;
            entity.alive = false;
            this.next_timing_attack = 0;
        } else {
            entity.health -= damage;
        }
    }

    canAttack() {
        return this.next_timing_attack <= 0;
    }

    pwidth() {
        return this.alive ? (this.next_timing_attack / this.speed_attack / 1000 * 100) + "%" : "0%";
    }

    clone() {
        return new Entity(this.name, this.maxHealth, this.damage, this.speed_attack);
    }

    reset() {
        this.next_timing_attack = this.speed_attack * 1000;
    }

}