import {clamp} from '../tmath'

export class Player {

    /**
     * Le personnage que le joueur joue
     * @type {Character}
     */
    character = null;

    dungeon = 0;
    floor = 0;

    in_fight = false;

    delay_heal = 0;
    delay_rest = 0;
    delay_eat = 0;

    constructor(character) {
        this.character = character;
    }

    attack(entity) {
        this.character.attack(entity);
    }

    canAttack() {
        return this.character.canAttack();
    }

    heal() {
        this.delay_heal = 2 * 1000;
        this.character.heal();
    }

    rest() {
        this.delay_rest = 15 * 1000;
        this.character.rest();
    }

    eat() {
        this.delay_eat = 15 * 1000;
        this.character.eat();
    }

    in_delay() {
        return this.delay_rest > 0 || this.delay_eat > 0 || this.delay_heal > 0;
    }

    btn_delay(index) {
        switch (index) {
            case 0:
                return this.delay_rest / 10 / 15 + "%";
            case 1:
                return this.delay_eat / 10 / 15 + "%";
            case 2:
                return this.delay_heal / 10 / 2 + "%";
            default:
                return "0%";
        }
    }

    updateGeneric(frame) {
        this.character.updateGeneric(frame);

        if(this.in_delay()) {
            console.log(this.delay_rest);
            this.delay_rest = clamp(this.delay_rest - frame, 0, 10**10);
            this.delay_eat = clamp(this.delay_eat - frame, 0, 10**10);
            this.delay_heal = clamp(this.delay_heal - frame, 0, 10**10);
        }
    }
}