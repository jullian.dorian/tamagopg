import {Quiz} from "./quiz/quiz";
import {Dungeon} from "./world/dungeon";
import {Entity} from "./entity/entity";
import {Floor} from "./world/floor";
import Spawner from "./world/spawner";
import {celsius, wind} from "./tmath";

export class Game {

    /**
     * Le quiz en début de partie pour choisir le personnage
     * @type {Quiz}
     */
    quiz = new Quiz();

    /**
     * Le joueur qui joue
     * @type {Player}
     */
    player;
    in_game = true;
    win = false;

    /**
     * Liste des alliés
     * @type {Warrior|Slayer|Mage[]}
     */
    allies = [];

    /**
     * Liste des donjons
     * @type {Dungeon[]}
     */
    dungeons = [];

    /**
     * Liste des entités
     * @type {Entity[]}
     */
    entities = [];

    update_character;
    update_fight;

    meteo;

    constructor() {
        this.loadDefault();
    }

    loadDefault() {
        /* --- Entity --- */
        this.entities.push(
            new Entity("Slime", 5, 1, 1.5),
            new Entity("Slime renforcé", 10, 1, 1.75),
            new Entity("Slime aggressif", 5, 2, 1.25),
            new Entity("King Slize", 20, 4, 1.25),

            new Entity("Petite araignée", 10, 2, 0.75),
            new Entity("Araignée salée", 12, 3, 1),
            new Entity("SpidyCochon", 15, 4, 1.25),
            new Entity("Arai'Mama", 30, 8, 2),

            new Entity("Champipouf", 20, 4, 2.25),
            new Entity("Champistroumpf", 15, 3, 1.5),
            new Entity("Tête de champi", 10, 7, 1.75),
            new Entity("Champi vénéneux", 40, 8, 1.5),

            new Entity("Literalkelebt", 20, 5, 1.33),
            new Entity("Dragon mineur", 40, 6, 1.25),
            new Entity("Dragon immature", 60, 7, 1),
            new Entity("Dragon de feu", 80, 10, 0.75)
        );


        /* --- Dungeon --- */
        let plains = new Dungeon("Plaines Visqueuses");
        plains.addFloor(new Floor(4, [new Spawner(this.entities[0], 1, 2)], 2));
        plains.addFloor(new Floor(4, [new Spawner(this.entities[1], 1, 2)], 2));
        plains.addFloor(new Floor(4, [new Spawner(this.entities[2], 1, 2)], 2));
        plains.addFloor(new Floor(6, [new Spawner(this.entities[0], 1, 2),
            new Spawner(this.entities[1], 1, 2),
            new Spawner(this.entities[2], 1, 2)], 1));
        plains.addFloor(new Floor(1, [new Spawner(this.entities[3], 1, 1)], 1, true));

        let forest = new Dungeon("Forêt Enchanté");
        forest.addFloor(new Floor(6, [new Spawner(this.entities[4], 3, 5)], 2));
        forest.addFloor(new Floor(4, [new Spawner(this.entities[5], 2, 4)], 2));
        forest.addFloor(new Floor(4, [new Spawner(this.entities[6], 2, 4)], 2));
        forest.addFloor(new Floor(6, [new Spawner(this.entities[4], 3, 5),
            new Spawner(this.entities[5], 1, 2),
            new Spawner(this.entities[6], 1, 2)], 1));
        forest.addFloor(new Floor(1, [new Spawner(this.entities[7], 1, 1)], 1, true));

        let cave = new Dungeon("Grotte ensevelis");
        cave.addFloor(new Floor(3, [new Spawner(this.entities[8], 2, 3)], 2));
        cave.addFloor(new Floor(5, [new Spawner(this.entities[9], 2, 4)], 2));
        cave.addFloor(new Floor(5, [new Spawner(this.entities[10], 2, 4)], 2));
        cave.addFloor(new Floor(6, [new Spawner(this.entities[8], 2, 2),
            new Spawner(this.entities[9], 2, 2),
            new Spawner(this.entities[10], 2, 2)], 1));
        cave.addFloor(new Floor(1, [new Spawner(this.entities[11], 1, 1)], 1, true));

        let moutain = new Dungeon("Montagne d'Erebor");
        moutain.addFloor(new Floor(2, [new Spawner(this.entities[12], 1, 2)], 2));
        moutain.addFloor(new Floor(2, [new Spawner(this.entities[13], 1, 2)], 1));
        moutain.addFloor(new Floor(2, [new Spawner(this.entities[14], 1, 2)], 1));
        moutain.addFloor(new Floor(1, [new Spawner(this.entities[15], 1, 1)], 1, true));

        this.dungeons = [plains, forest, cave, moutain];
    }

    init() {
        this.in_game = true;
        this.getDungeonFloor().generate();
        this.win = false;

        console.log(this.player);

        let frame = 100;
        this.update_character = setInterval(() => {
            if(this.player) {
                this.player.updateGeneric(frame);
                this.allies.forEach(a => a.updateGeneric(frame));

                if (!this.player.character.alive) {
                    this.end();
                }

            }
        }, 100);
    }

    start() {

        if(!this.player.character.alive)
            return;

        this.player.in_fight = true;

        let frame = 50;
        this.update_fight = setInterval(() => {
            //Mise à jour du temps d'attaque
            this.player.character.update(frame);
            this.allies.forEach(a => {
                a.update(frame);
            })

            //Gestion de l'attaque
            if(this.player.canAttack()) {
                this.player.attack(this.getDungeonFloor().entities);
            }
            this.allies.forEach(a => {
                if(a.canAttack() && a.alive) {
                    a.attack(this.getDungeonFloor().entities);
                }
            })

            //Mise à jour de l'attaque
            this.getDungeonFloor().entities.forEach(e => {
                e.update(frame);
            });

            //Gestion de l'attaque des entités
            let persons = [];
            persons.push(this.player.character);
            this.allies.forEach(a => {
                persons.push(a);
            })

            this.getDungeonFloor().entities.forEach(e => {
                if(e.canAttack() && e.alive) {
                    e.attack(persons);
                }
            });

            //Update du floor
            this.getDungeonFloor().update();
            //Si l'étage est fini
            if(this.getDungeonFloor().complete) {
                //Si l'étage est encore répétable

                clearInterval(this.update_fight);
                this.player.in_fight = false;
                this.clearAttack();


                //Si le donjon est répétable
                if(this.getDungeonFloor().repeatable > 1) {
                    this.getDungeonFloor().repeatable -= 1;
                    this.getDungeonFloor().step += 1;
                    //On regenère le donjon
                    this.getDungeonFloor().generate();
                }
                //Cela signifie que l'étage est entièrement clean on passe au suivant
                else {
                    //On vérifie qu'on peut passer à l'étage suivant
                    if(this.getDungeon().next(this.player.floor)) {
                        this.player.floor += 1;
                        //On regenère le donjon
                        this.getDungeonFloor().generate();

                        this.player.character.upgrade();
                        this.allies.forEach(a => a.upgrade());
                    }
                    //On change de donjon
                    else {
                        this.player.floor = 0;
                        this.player.dungeon += 1;

                        //Si on a pas atteint la limite
                        if(this.player.dungeon <= this.dungeons.length) {
                            this.getDungeonFloor().generate();
                        } else {
                            this.win = true;
                            this.end();
                        }
                    }
                }
            }

            if(!this.player.character.alive) {
                this.end();
            }
        }, frame);
    }

    end() {
        this.clearAttack();
        clearInterval(this.update_character);
        clearInterval(this.update_fight);
        this.in_game = false;
    }

    ambiant(meteo) {
        let m = {
            name: meteo.name,
            humidity: meteo.main.humidity,
            temp: celsius(meteo.main.temp),
            wind: wind(meteo.wind.speed)
        };

        this.meteo = m;
    }

    getDungeon() {
        return this.dungeons[this.player.dungeon];
    }

    getDungeonFloor() {
        return this.getDungeon().floors[this.player.floor];
    }

    generateFloor() {
        this.player.character.effects(this.getDungeon(), this.meteo);
        this.allies.forEach(a => a.effects(this.getDungeon(), this.meteo));

        this.getDungeonFloor().generate();

        this.getDungeonFloor().entities.forEach(e => e.effects(this.getDungeon(), this.meteo))
    }

    getFloor() {
        return 1 + this.player.floor + (this.getDungeonFloor().step / 2);
    }

    clearAttack() {
        this.player.character.reset();
        this.allies.forEach(a => a.reset());
        this.getDungeonFloor().entities.forEach(e => e.reset());
    }

    rest() {
        if(!this.player.in_delay()) {
            this.player.rest();
            this.allies.forEach(a => a.rest());
        }
    }

    eat() {
        if(!this.player.in_delay()) {
            this.player.eat();
            this.allies.forEach(a => a.eat());
        }
    }

    heal() {
        if(!this.player.in_delay()) {
            this.player.heal();
            this.allies.forEach(a => a.heal());
        }
    }
}