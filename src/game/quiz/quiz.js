import {Characters, Mage, Slayer, Warrior} from "../entity/character";

export class Quiz {

    index = 0;
    questions = [];
    responses = [];
    complete = false;

    constructor(questions = []) {
        if(questions === undefined || questions === null || questions.length === 0)
            this.loadDefault();
        else
            this.questions = questions;
    }

    loadDefault() {
        this.questions = [
            new QQuestion("Aimez-vous foncer vers vos ennemis", [
                new QResponse("POUR SPARTIATE !", Characters.TYPES.WARRIOR), //Guerrier
                new QResponse("Seulement quand c'est necessaire", Characters.TYPES.SLAYER), //Assassin
                new QResponse("Je devrais aller cueillir des fleurs...", Characters.TYPES.MAGE)//Mage
            ]),
            new QQuestion("Aimez-vous les comtes de fée", [
                new QResponse("Luminos Maxima ~", Characters.TYPES.MAGE), //Mage
                new QResponse("Maman regarde ! Une libélulle !", Characters.TYPES.WARRIOR),//Guerrier
                new QResponse("Je regarde dans les ténèbres et les ténèbres regardent en moi", Characters.TYPES.SLAYER) //Assassin
            ]),
            new QQuestion("Que pensez-vous du film 'Le Seigneur des Anneaux'", [
                new QResponse("Le quoi ?", Characters.TYPES.SLAYER), //Assassin
                new QResponse("Vous ne passerez pas !",Characters.TYPES.MAGE), //Mage
                new QResponse("Il faut se rendre à Erebor !",Characters.TYPES.WARRIOR)//Guerrier
            ]),
            new QQuestion("Si vous aviez la possibilité de voyager dans le temps, quel arme prendriez-vous", [
                new QResponse("Mon couteau de cuisine", Characters.TYPES.SLAYER), //Assassin
                new QResponse("Mes gants de boxe", Characters.TYPES.WARRIOR), //Guerrier
                new QResponse("De quoi écrire", Characters.TYPES.MAGE) //Mage
            ])
        ];
    }

    reset() {
        this.responses = [];
        this.complete = false;
        this.index = 0;
    }

    getQuestion() {
        return this.questions[this.index];
    }

    nextQuestion(response) {
        //Ajoute les points
        this.responses.push(response);

        this.index++;
        let end = this.index < this.questions.length;
        this.complete = !end;
        return end;
    }

    /**
     *
     * @param name
     * @returns {{allies: (Mage|Slayer|Warrior)[], playable: Warrior|Mage|Slayer}}
     */
    buildCharacter(name) {
        let who = Characters.calculate(this.responses);
        if(who === Warrior) {
            return {
                playable: new Warrior(name, 40, 5),
                allies: [
                    new Mage("Array Pauter", 20, 3, 25),
                    new Slayer("Théo Den", 16, 5, 2.85)
                ]
            };
        } else if(who === Mage) {
            return {
                playable: new Mage(name, 25, 3, 50),
                allies: [
                    new Warrior("Gah Ren", 35, 5),
                    new Slayer("Théo Den", 16, 5, 2.85)
                ]
            };
        } else
            return {
                playable: new Slayer(name, 20, 5, 2.75),
                allies: [
                    new Warrior("Gah Ren", 35, 5),
                    new Mage("Array Pauter", 20, 3, 25)
                ]
            };
    }
}

export class QQuestion {

    title;
    responses;

    constructor(question, responses) {
        this.title = question;
        this.responses = responses;
    }
}

export class QResponse {

    response;
    point;

    constructor(response, point) {
        this.response = response;
        this.point = point;
    }
}