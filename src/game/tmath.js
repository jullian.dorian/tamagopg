export function minmax(min, max) {
    if(min < max) return min;
    return max;
}

export function maxmin(min, max) {
    if(max > min) return max;
    return min;
}

export function clamp(value, min, max) {
    return Math.max(min, Math.min(value, max));
}

export function affine(value) {
    return Math.round(value * 100) / 100;
}

export function celsius(kelvin) {
    return Math.round((kelvin - 273.15) * 100) / 100;
}

export function wind(wind_sec) {
    return Math.round((wind_sec * 3.60) * 100)/100;
}