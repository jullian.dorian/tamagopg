export class Dungeon {

    name;
    floors = [];

    in_cave;
    in_moutain;

    constructor(name, cave = false, moutain = false) {
        this.name = name;
        this.in_cave = cave;
        this.in_moutain = moutain;
    }

    addFloor(floor) {
        this.floors.push(floor);
    }

    next(floorId) {
        return floorId + 1 < this.floors.length;
    }
}