import {minmax} from "../tmath";

export class Floor {

    max_qtt;
    /**
     * @type {Spawner[]}
     */
    spawners;
    repeatable;
    boss;
    step = 0;

    entities = [];

    complete = false;

    constructor(number, spawners, repeat,boss = false) {
        this.max_qtt = number;
        this.spawners = spawners;
        this.repeatable = repeat;
        this.boss = boss;
    }

    generate() {
        this.entities = [];
        let rand = Math.ceil(Math.random() * this.max_qtt);

        while(rand > 0) {

            let sp = this.spawners[Math.floor(Math.random() * this.spawners.length)];

            if(sp) {
                let qtt = Math.random() * (sp.max_qtt - sp.min_qtt) + sp.min_qtt;

                let amount = minmax(qtt, rand);

                rand -= amount;

                this.entities.push(sp.entity.clone());
            }
        }
    }

    update() {
        let valid = true;
        this.entities.forEach(e => {
            valid &= !e.alive;
        });

        this.complete = valid;
    }

    monsters() {
        return this.spawners.map(s => s.entity.name).join(", ");
    }

}