import {maxmin, minmax} from "../tmath";

export default class Spawner {

    entity;
    min_qtt;
    max_qtt;

    constructor(entity, number, number2) {
        this.entity = entity;
        this.min_qtt = minmax(number, number2);
        this.max_qtt = maxmin(number, number2);
    }
}